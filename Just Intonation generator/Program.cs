﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Just_Intonation_generator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("                              ----------Just intonation generator----------");
            Console.WriteLine("The purpose of this program is to generate usable ratios for just intonation");
            Console.Write("Remember: The last ratio should always be 2/1");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Generate? (Y/N):");
            Console.WriteLine();

            var userinput = Console.ReadLine();
            if (userinput == "y")
            {
                Console.WriteLine("Your ratios are:");
                Console.WriteLine();
                int min = 1;
                int max = 50;

                int[] scalearray1 = new int[11];
                int[] scalearray2 = new int[11];

                Random randNum = new Random();
                for (int i = 0; i < scalearray1.Length; i++)
                    {
                        scalearray1[i] = Convert.ToInt32(randNum.Next(min, max));
                        scalearray2[i] = Convert.ToInt32(randNum.Next(min, max));

                    {
                        if (scalearray1[i] > scalearray2[i] && scalearray1[i] < scalearray2[i] + scalearray2[i])
                            {
                                Console.WriteLine(scalearray1[i] + ":" + scalearray2[i]);
                            }
                        else
                            {
                                i--;
                            }
                    }

                }
                Console.Write("Generate more values? (Y/N): ");
            }   
            Console.WriteLine();
            
            Console.WriteLine();

            Console.ReadLine();                                                                             

        }
    }
}
